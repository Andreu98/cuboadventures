﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plataforma_movimiento_1 : MonoBehaviour
{
    
   public Vector3 velocidad;
   public float tiempocambio;
   private void Start() {
       StartCoroutine(movimientoX());
   }

    
    IEnumerator movimientoX(){

      //  Debug.Log("nueva corrutina");
//        Debug.Log(velocidad+" antes");
        velocidad=new Vector3(velocidad.x*-1,velocidad.y*-1,velocidad.z*-1);
        this.GetComponent<Rigidbody>().velocity=velocidad;
//        Debug.Log(velocidad+" despues");
        yield return new WaitForSeconds(tiempocambio);
        StartCoroutine(movimientoX());
    }
}

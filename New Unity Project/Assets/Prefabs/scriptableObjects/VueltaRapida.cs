﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "VueltaRapida", menuName = "VueltaRapidaAgain", order = 52)]
public class VueltaRapida : ScriptableObject
{
    [SerializeField]
    private float VR;
    public float vueltaRapida { get; set; }

}

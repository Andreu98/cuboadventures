﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plataforma_controller : MonoBehaviour
{
   
    
    public float velocidadAscenso;
    public float velocidadDescenso;
    public float segundosSubida;
    public float comienzoDescenso;
   

    private void OnCollisionEnter(Collision other) {
        
        Debug.Log(other.gameObject.tag);
        if(other.gameObject.CompareTag("Player")){
            if(this.gameObject.GetComponent<Rigidbody>().isKinematic){
                //Si el player vuelve a la plataforma cuando estaba en corrutina, detiene la corrutina de descenso
                //Deberia hacer un if para comprobar al direccion de la velocidad del eje y, si es negativa, no deberia estabilizarse
                StopCoroutine(cuentaAtras());
            }else{
                //si no hay corrutina, sube
            traslacion();
            }
        }

        if(other.gameObject.CompareTag("soporte")){
             if(this.gameObject.GetComponent<Rigidbody>().isKinematic==false){
                
                StopCoroutine(cuentaAtras());
                this.gameObject.GetComponent<Rigidbody>().isKinematic=true;
            }
        }
    } 
    
    private void OnCollisionExit(Collision other) {
        if(other.gameObject.CompareTag("Player")){
            StopCoroutine(ascenso());
            bajarPlataforma();
        }
    }
    /// <summary>
    /// Comienza el movimiento ascendente
    /// </summary>
    public void traslacion(){
        this.gameObject.GetComponent<Rigidbody>().velocity=(new Vector3(0,velocidadAscenso,0));
        StartCoroutine(ascenso());
        
    }
    /// <summary>
    /// Comienza el movimiento descendente
    /// </summary>
    public void bajarPlataforma(){
        StartCoroutine(cuentaAtras());
        
    }
    /// <summary>
    /// Cuando pasan los segundos especificados en la variable comienzoDescenso, el objeto ya no es kinematico y comienza a bajar
    /// </summary>
    /// <returns></returns>
    IEnumerator cuentaAtras(){
        
        yield return new WaitForSeconds(comienzoDescenso);
        this.gameObject.GetComponent<Rigidbody>().isKinematic=false;
        this.gameObject.GetComponent<Rigidbody>().velocity=(new Vector3(0,-velocidadDescenso,0));
        Debug.Log(this.gameObject.GetComponent<Rigidbody>().isKinematic);
    }

    /// <summary>
    /// Cuando a subido los segundos especificados, kinematic=true
    /// </summary>
    /// <returns></returns>
      IEnumerator ascenso(){
        
        yield return new WaitForSeconds(segundosSubida);
        this.gameObject.GetComponent<Rigidbody>().isKinematic=true;
    }


    
}

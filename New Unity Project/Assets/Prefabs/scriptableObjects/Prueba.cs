﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Prueba : MonoBehaviour
{
    private float timeIni;
    private float timeEnd;
    [SerializeField]
    public VueltaRapida vueltaRapida;
   // public static float FIN;

    private void Start()
    {
        timeIni = Time.time;
    }

     void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            timeEnd = Time.time;
            if (timeEnd - timeIni < vueltaRapida.vueltaRapida)
            {
                vueltaRapida.vueltaRapida = timeEnd - timeIni;
                Debug.Log(timeEnd);
                Debug.Log(timeIni);
            }
            PlayerPrefs.SetFloat("TIME", vueltaRapida.vueltaRapida);
            SceneManager.LoadScene("Win");
        }
    }


}

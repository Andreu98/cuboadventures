﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinternaController : MonoBehaviour
{
    public float MaxTime=12;
    public float lifeTime;
    public Light light;
    public Mode mode=Mode.OFF;
    
    public enum Mode {
        ON,OFF,FLICKER,EMPTY
    }
    private void Awake() {
        light=this.GetComponent<Light>();
        light.enabled=false;
        lifeTime=MaxTime;
    }
    //Light Inputs
    private void Update() {

        //Debug.Log(this.GetComponent<Transform>().localRotation);
        if(Input.GetKeyDown("c")){

            if(mode==Mode.ON||mode==Mode.FLICKER){
                mode=Mode.OFF;
                Off();
                
                
            }else if(mode==Mode.OFF){
                mode=Mode.ON;
                On();
            }
        }
        if (Input.GetKey("up"))
        {
            upPressed();
        }
        if (Input.GetKey("down"))
        {
            downPressed();
        }
        if (Input.GetKey("left"))
        {
            
            leftPressed();
        }
        if (Input.GetKey("right"))
        {
            rightPressed();
        }

    }
    #region Mode Changes
    private void On(){
        light.enabled=true;
        StartCoroutine(toFlicker(lifeTime));

    }

    private void Off(){
        light.enabled=false;
        StopAllCoroutines();
        lifeTime = MaxTime;
        mode = Mode.OFF;

    }
    private void Flicker(){
        mode=Mode.FLICKER;
        StartCoroutine(flicking(lifeTime));
    }
    #endregion
    #region CORRUTINES
    IEnumerator toFlicker(float lifeTime){

        float espera = lifeTime-5;
        yield return new WaitForSeconds(espera);
        this.lifeTime=5;
        Flicker();
    }
    IEnumerator flicking(float lifeTime){
        float freq = 0.15f;
        while(lifeTime>0){
            if(light.enabled==true){
            yield return new WaitForSeconds(freq);
            light.enabled=false;
            lifeTime-=0.15f;
            } else {
                yield return new WaitForSeconds(freq);
                light.enabled=true;
                lifeTime-=0.15f;
            }
        }
        Debug.Log("Sin bateria");
        light.enabled=false;
        mode=Mode.EMPTY;
        lifeTime = 0;
        StartCoroutine(recharge());
    }
    IEnumerator recharge (){
        
        yield return new WaitForSeconds(MaxTime);
        Debug.Log("recargado");
        lifeTime=MaxTime;
        mode=Mode.OFF;
    }
    #endregion
    #region Rotation
    private void upPressed()
    {
        print(this.transform.rotation.eulerAngles.x + "  " + this.transform.rotation.eulerAngles.y + " " + this.transform.rotation.eulerAngles.z);
        //this.transform.Rotate(-this.transform.right, Time.deltaTime + 0.5f);

    }
    private void downPressed()
    {
        print(this.transform.rotation.eulerAngles.x + "  " + this.transform.rotation.eulerAngles.y + " " + this.transform.rotation.eulerAngles.z);
       // this.transform.Rotate(this.transform.right, Time.deltaTime + 0.5f);
    }
    private void leftPressed()
    {
        //para angulo de -30 a 30 hay que hacer de 30 a 330 o algo asi, usar los euler
        print(this.transform.rotation.eulerAngles.x+"  "+this.transform.rotation.eulerAngles.y+" "+this.transform.rotation.eulerAngles.z);
        if (!(this.transform.rotation.eulerAngles.y > 315 && this.transform.rotation.eulerAngles.y <330) )
        { // && this.transform.rotation.eulerAngles.y < 360 )
            this.transform.Rotate(-this.transform.up, Time.deltaTime + 1.5f);
        }
    }
    private void rightPressed()
    {
       
        if (!(this.transform.rotation.eulerAngles.y < 65 && this.transform.rotation.eulerAngles.y > 40))
        {
            this.transform.Rotate(this.transform.up, Time.deltaTime + 1.5f);
        }
    }
    #endregion
    #region Triggers
    private void OnTriggerEnter(Collider other) {
        

            if(other.gameObject.tag=="Enemigo"){
                if(this.mode!=Mode.OFF&&this.mode!=Mode.EMPTY){
                    Debug.Log("llamada a contacto luz");
                    other.gameObject.GetComponent<EnemigoController>().contactoLuz();
                }
            }
        
    }

    private void OnTriggerExit(Collider other) {
        if(other.gameObject.tag=="Enemigo"){
            Debug.Log("llamada a fuera luz en trigger exit");
            other.gameObject.GetComponent<EnemigoController>().fueraLuz();
        }
        
    }

    private void OnTriggerStay(Collider other) {
        
        

            if(other.gameObject.tag=="Enemigo"){
                 if(this.mode==Mode.OFF||this.mode==Mode.EMPTY){
                     
                   //Debug.Log("llamada a fuera luz en trigger stay");
                    other.gameObject.GetComponent<EnemigoController>().fueraLuz();
                }else if(!other.GetComponent<EnemigoController>().corrutinaActiva) {
                    //Debug.Log("HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA");
                    other.gameObject.GetComponent<EnemigoController>().contactoLuz();
                }
            }
        
    }
    #endregion
    //Izquierda y derecha: eje y
    //arriba y abajo: eje x

}

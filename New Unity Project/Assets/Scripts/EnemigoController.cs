﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemigoController : MonoBehaviour

{
    public float segundosMuerte;
    public bool corrutinaActiva=false;
    public GameObject player;
    bool perseguir = false;
   public float sp=0.00000000000000000000001f;
    private void Awake()
    {
        
    }
    private void FixedUpdate()
    {
        if (perseguir)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, player.transform.position, sp);
        }
    }

    //Tanto contactoLuz como fueraLuz se llaman desde LinternaController -> region de triggers
    public void contactoLuz(){
        StartCoroutine(corrutinaEnter());
    }
    public void fueraLuz(){
        
        StopCoroutine(corrutinaEnter());
        corrutinaActiva=false;
    }
    IEnumerator corrutinaEnter(){
        corrutinaActiva=true;
        yield return new WaitForSeconds (segundosMuerte);
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            perseguir = true;
            
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("Juego");

        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameController : MonoBehaviour
{
    public Camera mainCamera;
    public PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mainCamera.transform.position = new Vector3
		(
            //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
			Mathf.Lerp(mainCamera.transform.position.x, (player.transform.position.x - (player.transform.forward.x * 10)), Time.deltaTime * 10),
			Mathf.Lerp(mainCamera.transform.position.y, (player.transform.position.y - (player.transform.forward.y * 10)+6), Time.deltaTime * 10),
			Mathf.Lerp(mainCamera.transform.position.z, (player.transform.position.z - (player.transform.forward.z * 10)), Time.deltaTime * 10)
		);
          //fa una rotació automàtica per a que miri al jugador
		mainCamera.transform.LookAt(player.transform);


    }
}
